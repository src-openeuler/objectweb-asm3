%global asm_list asm asm-analysis asm-commons asm-tree asm-util asm-xml asm-all

Name:         objectweb-asm3
Version:      3.3.1
Release:      22
Summary:      A bytecode manipulation framework for Java
License:      BSD-3-Clause and Apache-2.0
URL:          http://asm.ow2.org/
Source0:      http://download.forge.ow2.org/asm/asm-%{version}.tar.gz
Source1:      http://www.apache.org/licenses/LICENSE-2.0.txt
BuildArch:    noarch

BuildRequires:ant maven-local objectweb-asm3
Requires:     javapackages-tools

%description
ASM is a general Java bytecode manipulation and analysis framework. It can be used directly in
binary form to modify existing classes or dynamically generate classes. Provides universal
transformation and analysis algorithms to easily assemble custom complex transformation and
code analysis tools.

%package help
Summary:   Objectweb-asm3 API documentation
Provides:  %{name}-javadoc = %{version}-%{release}
Obsoletes: %{name}-javadoc < %{version}-%{release}

%description  help
This package provides help documents for objectweb-asm3.

%prep
%autosetup -n asm-%{version} -p1
find -name *.jar -delete
sed -i "/Class-path/d;s/org.objectweb.asm.all/org.objectweb.asm/" archive/asm-xml.xml

%build
%ant -Dobjectweb.ant.tasks.path= jar jdoc
mv output/dist/lib/all/* output/dist/lib/

for var in %{asm_list}; do
    cp output/dist/lib/${var}-%{version}.pom output/dist/lib/${var}-distroshaded-%{version}.pom
    %pom_xpath_set "pom:project/pom:artifactId" "${var}-distroshaded" \
                   output/dist/lib/${var}-distroshaded-%{version}.pom
done

pushd  output/dist/lib
lists="asm-analysis asm-commons asm-util"
for var in ${lists}; do
    %pom_remove_dep :asm-tree ${var}-distroshaded-%{version}.pom
    %pom_add_dep asm:asm-tree-distroshaded:3.3.1 ${var}-distroshaded-%{version}.pom
done
%pom_remove_dep :asm-util asm-xml-distroshaded-%{version}.pom
%pom_add_dep asm:asm-util-distroshaded:3.3.1 asm-xml-distroshaded-%{version}.pom
%pom_remove_dep :asm asm-tree-distroshaded-%{version}.pom
%pom_add_dep asm:asm-distroshaded:3.3.1 asm-tree-distroshaded-%{version}.pom
popd

for var in %{asm_list}; do
    shade-jar org.objectweb.asm org.objectweb.distroshaded.asm output/dist/lib/${var}-%{version}.jar \
              output/dist/lib/${var}-distroshaded-%{version}.jar
    jar xf output/dist/lib/${var}-distroshaded-%{version}.jar META-INF/MANIFEST.MF
    sed -i /Bundle-/d META-INF/MANIFEST.MF
    jar ufM output/dist/lib/${var}-distroshaded-%{version}.jar META-INF/MANIFEST.MF
done

%install
%mvn_artifact output/dist/lib/asm-parent-%{version}.pom

for var in %{asm_list}; do

    %mvn_artifact output/dist/lib/${var}-distroshaded-%{version}.pom \
                  output/dist/lib/${var}-distroshaded-%{version}.jar
    %mvn_artifact output/dist/lib/${var}-%{version}.pom output/dist/lib/${var}-%{version}.jar
done
%mvn_install -J output/dist/doc/javadoc/user

%jpackage_script org.objectweb.asm.xml.Processor "" "" %{name}/asm:%{name}/asm-attrs:%{name}/asm-util:%{name}/asm-xml %{name}-processor true

%files -f .mfiles
%doc LICENSE.txt README.txt
%{_bindir}/%{name}-processor
%dir %{_javadir}/%{name}

%files help -f .mfiles-javadoc

%changelog
* Tue May 10 2022 caodongxia <caodongxia@h-partners.com> - 3.3.1-22
- License compliance rectification

* Sat Dec 21 2019 duyeyu <duyeyu@huawei.com> - 3.3.1-21
- Initial packaging
